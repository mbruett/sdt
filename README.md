## SDT-Analyse: Kurzanleitung

*Stand: 14.06.2021*

Im Folgenden sei ein kurzer Überblick über die grundlegende Funktionsweise der aktuellen Version des SDT-Analysetools in MATLAB gegeben. Diese Kurzanleitung soll dabei helfen, das Programm aufzusetzen, zu bedienen und einen Abriss über die grundsätzliche Funktionsweise geben.

[TOC]


### <a name="1" style="color:black">1 Installation</a>

Die aktuelle Version des SDT-Analysetools wird über den AG-Cloud-Server der Arbeitsgruppe Allgemeine Psychologie I zugänglich gemacht. Die Software sollte grundsätzlich von hier bezogen werden, um zu gewährleisten, dass sie auf dem aktuellesten Stand ist. 

Zur Installation genügt es, den vollständigen Ordner, in dem die Skripte abgelegt sind, an einen Ort auf dem eigenen Rechner zu kopieren. Zur Ausführung der Skripte werden dann einige Programme benötigt, die unten aufgeführt sind.

#### <a name="1.1" style="color:black">1.1 Voraussetzungen</a>

Um das SDT-Analysetool einzusetzen, werden eine folgende Programme benötigt:

- MATLAB in der Version R2017a oder höher[^1]

- Microsoft Excel in der Version 2007 oder neuer[^2]

  [^1]: Ältere Versionen sind nicht unterstützt, können aber funktionieren. Momentan wird die aktuellste Version von MATLAB über die Uni kostenlos für Studierende und Mitarbeiter angeboten.
  [^2]: Getestet für Windows-Rechner. Das Tool wurde auf anderen Betriebssystemen nicht getestet und diese sind damit bis auf Weiteres nicht unterstützt.

Die vorliegende Skriptversion wurde mit MATLAB R2017a bis MATLAB 2021a getestet und erzeugt mit älteren Versionen möglicherweise Kompatibilitätsprobleme.

Zusätzlich ist MS Excel 2007 oder höher zur Benutzung notwendig, da Daten und Ergebnisse im XLSX-Format gehandhabt werden, wofür MATLAB intern MS Excel verwendet. Ein Open-Source-Ersatz wie bspw. Libre Office ist daher nicht ausreichend.

#### <a name="1.2" style="color:black">1.2 Verzeichnisstruktur</a>

Die auf MATLAB basierende Version sollte momentan mitsamt ihrer kompletten Ordnerstruktur zugänglich sein. Diese Ordnerstruktur ist notwendig für die Ausführung des Programmes und sei deswegen an dieser Stelle zur Kontrolle kurz aufgeführt:

> log</br>
>
> plot</br>
>
> private</br>
>
> ┕ `analytic.m`</br> 
>
> ┕ `fit.m` </br>
>
> ┕ `get_data.m` </br>
>
> ┕ `get_model_logli.m` </br>
>
> ┕ `get_modelanswer.m` </br>
>
> ┕ `getcrit.m` </br>
>
> ┕ `getstart.m` </br>
>
> ┕ `liho.m` </br>
>
> ┕ `lihora_minus_one.m` </br>
>
> ┕ `likelihood_ratio_test.m` </br>
>
> ┕ `maxparam_lr_test.m`</br>
>
> results</br>
>
> xlslog</br>
>
> ┕ `_exampledata.xlsx` </br>
>
> `sdtbatch.m`</br>
>
> `do.m`</br>

Insbesondere während der Entwicklung können alle Ordner Dateien enthalten, die hier nicht aufgeführt sind. Diese sind jedoch i.A. zur Benutzung des Programmes nicht notwendig.

<!-- Erklärung der Verzeichnisstruktur -->



### <a name="2" style="color:black">2 Benutzung</a>

Die korrekte Benutzung des Programmes hängt in erster Linie an der Einhaltung des speziellen Datenformates. Für einfache Anwendungszwecke reicht danach die Ausführung des beiliegenden ``sdtbatch.m``-Skriptes, auf das im Folgenden kurz eingegangen werden soll.
Weiter unten soll [in Zukunft] eine Übersicht über die einzelnen Bestandteile des Programmes gegeben werden.

#### <a name="2.1" style="color:black">2.1 Datenformat</a>

<!-- Aufteilen in Daten, Theorien usw. -->

Zur Benutzung des Programmes ist es zunächst wichtig, dass ein spezielles Format in den benutzten Datensätzen eingehalten wird. Zur Erklärung diesen Formates ist im Verzeichnis ``xlslog`` eine Datei namens  ``_exampledata.xlsx`` gegeben. In dieser werden mit farblichen Markierungen alle legalen Eintragungen anhand eines Beispieldatensatzes erklärt. Die farblichen Markierungen selbst sind hierbei für die Bedienung des Programmes irrelevant, Kommentare (eingeleitet mit dem Symbol ``%``) werden vom Programm ebenfalls ignoriert.

<!-- Einbindung einer Erklärgrafik -->

Zum Testen von Zusammenhängen der berechneten Modellparameter können Theorien spezifiziert werden (in der Form ``# [selbstgewählterName];[theorieArt]``), die auf den einzelnen Datenblättern unter den Daten selbst aufgeführt werden. Das Programm unterscheidet hier zwischen fünf verschiedenen Arten von Theorien:

| Typ     | Parameterzahl                                                | Erklärung                                                    |
| ------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| free    | 0                                                            | keine Theorieannahmen, freie Optimierung                     |
| linear  | = Anzahl der Spalten im Datensatz                            | Annahme der linearen Verteilung aller Stimuli zwischen erstem und letztem |
| power   | = Anzahl der Spalten im Datensatz                            | wie *linear*, nur mit zusätzlicher Berechnung eines Exponenten, der auf die Linearkoeffizienten gerechnet wird |
| given   | = Anzahl der Spalten im Datensatz                            | fest vorgegebene Mittelwerte für die Stimulusverteilungen    |
| absolut | = Anzahl der Spalten im Datensatz <br />   + Anzahl der Zeilen | wie *given*, nur dass die Kriterien zwischen den Stimuli auch fest vorgegeben werden |

Mindestens eine Theorie pro Datenblatt muss spezifiziert werden, um die Analyse durchzuführen.

Der dritte "Block" in jedem Datenblatt kann Likelihood-Ratio-Tests spezifizieren, die hierarchische Modelle gegeneinander testen (in der Form ``? [NameTheorie1];[NameTheorie2]``). Die Theorien, die den Modellen zugrundeliegen, müssen sich in ihren freien Parametern unterscheiden und auf denselben Parametern basieren (bspw. Linearkoeffizienten in einer Theorie vom Typ *linear* und einer vom Typ *power*). 
Ein LR-Test gegen das "Maximalparametermodell", dass auch die Annahme der Stimulusverteilungen und dazwischenliegenden Kriterien nicht macht, wird standardmäßig gerechnet und die zugehörigen Testergebnisse finden sich in ``[quelldateiname]_SDT_tab.xls`` (s. Abschnitt 2.2 für weitere Informationen).

#### <a name="2.2" style="color:black">2.2 Ausführung</a>

Vor der Ausführung des Skriptes sollten alle Instanzen von MS Excel, falls geöffnet, geschlossen werden, da Microsoft (zumindest in älteren Versionen der Office Suite) sonst bestimmte Befehle in Excel nicht ausführen kann und somit ggf. Probleme auftreten können. Ebenso sollte während des Analyse Excel nicht gestartet werden, also insbesondere keine XLS-Dateien geöffnet werden.

Die eigentliche Ausführung des Skriptes geschieht aus dem Basisverzeichnis (gleiche Ebene wie ``do.m``) mit einem Aufruf von

```matlab
sdtbatch(filename)
```

wobei ``filename`` der Name der Eingabedaten einschließlich Quellverzeichnis und Dateiendung ist. Ein Beispiel für eine Ausführung ist in ``do.m`` gegeben.

<!-- Bsp einbinden -->

Das Programm lädt nun die Daten aus der angegebenen Quelldatei und führt nacheinander die Modellpassung, die Modelltests und die Erzeugung der Plots (falls angegeben) durch. Die Konsole informiert dabei über die jeweils aktuellesten Ergebnisse der Analyse.

Sobald die Ausführung abgeschlossen ist, erzeugt das Programm drei Ausgabedateien:

* ``[quelldateiname]_SDT_tab.xls``
* ``[quelldateiname]_SDT_models.xls``
* ``[quelldateiname]_SDT_tests.xls``

Diese Dateien werden in ``results`` abgelegt und beinhalten die Modellparameter der berechneten optimalen SDT-Modelle, vorhergesagte Ergebnisse auf Basis dieser optimalen Modellparameter, beziehungsweise die Ergebnisse von Likelihood-Ratio-Tests, die im Quelldatensatz gefordert wurden (vgl. Abschnitt 2.1).

#### <a name="2.3" style="color:black">2.3 Programmübersicht</a>

[folgt]



### <a name="3" style="color:black">3 Problembehandlung</a>

Bei der Ausführung des Programmes kann es zu einer Fehlermeldung und einem daraus resultierenden Abbruch der Analyse kommen. Häufig lassen sich diese Fehler auf ein Kompatibilitätsproblem auf Seiten MATLABs zurückführen (vgl. Abschnitt 1.1). Sollte dies ausgeschlossen sein oder die Fehlermeldung selbst auf ein anderes Problem hinweisen, kann es auch helfen, MATLAB zu schließen und wieder zu öffnen, da einige Hintergrundprozesse noch laufen könnten. Aus demselben Grunde (zumindest unter MS Excel 2007 und 2017 repliziert) kann es nach einem Programmabbruch nötig sein, einen Hintegrundprozess von MS Excel mittels des Task Managers zu schließen, da MATLAB ihn u.U. geöffnet, aber nicht wieder geschlossen hat und deswegen in weiteren Durchläufen keinen Excel-Prozess mehr öffnen kann.

<!-- erklärende Grafik Task Manager -->

Ein weiterer möglicher Fehler ist ein "Invoke Error", den Excel über MATLAB ausgibt. Dieser kann wie folgt aussehen:

> Invoke Error, Dispatch Exception:
> Source: Microsoft Office Excel
> Description: Der eingegebene Name für ein Blatt oder Diagramm ist ungültig. Stellen
> Sie Folgendes sicher:
>
> • Der eingegebene Name weist maximal 31 Zeichen auf.
> • Der Name enthält keines der folgenden Zeichen:  :  \  /  ?  *  [  oder  ]

Zur Behebung ist zu überprüfen, ob die Namen der Tabellenblätter der Quelldatei, sowie die Namen der Modelltheorien (vgl. Abschnitt 2.1) möglichst kurz sind und nicht die Sonderzeichen ``\``, ``/``, ``:``, ``?``, ``[`` oder ``]`` enthalten. Bezüglich der Länge der Namen kann keine individuelle Obergrenze genannt werden (da die Namen intern neu kombiniert werden), jedoch sollte die Faustregel sein, dass genannte Namen nicht mehr als zehn alphanumerische Zeichen lang sein sollten (vgl. Abschnitt 2.1).

#### 3.1 Support

Das Tool befindet sich derzeit noch in der Entwicklung. Es kann demnach zu kleineren Unstimmigkeiten zwischen dieser Readme und den Skripten geben, ebenfalls können Fehler bei der Bedienung auftauchen. Fehlermeldungen und Fragen zur Benutzung, die auftauchen sollten, können an mich geschickt werden unter bruett@psychologie.uni-kiel.de. Für jede Anfrage, die mit einer spezifischen Ausführung der Skripte zusammenhängt, bitte ich um folgende Informationen:

- Betriebssystem, installierte MATLAB-Version
- Beschreibung des Fehlers, wenn möglich Textkopie oder Screenshot der Fehlermeldung(en)
- Ggf. gekürzte Version der Datenmappe (XLS-Datei), mit der die Analyse gescheitert ist
- Wenn möglich, die Logdatei, die bei der Ausführung generiert wurde. Die Logs finden sich im /log-Verzeichnis im Ordner des SDT-Tools und sind mit dem Namen der Datei und einem Zeitstempel versehen, um sie zuordnen zu können.

