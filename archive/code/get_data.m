function data_struct = get_data(filename, sheet, invertRows, invertCols)
%
%   GET_DATA( filename, sheet ) loads SDT data from a chosen xls file. The
%   data has to conform to certain rules as outlined in this program's
%   readme file.
%
%   ARGUMENTS:
%   sheet   -   Specify one or multiple sheets to extract. If empty ('') 
%               all sheets are extracted. Single sheet names have to be
%               given in single quotes, multiple sheets can be specified
%               via single quoted names inside a cell array (e.g. 
%               {'sheet1', 'sheet2'}). Empty sheets in the selection will
%               be ignored, a warning is given if done so.
%
%   RETURNS:
%   data_struct (as long as the number of sheets that have been loaded)
%    - name (of the data sheet)
%    - stimulus_names (equals column header of the data)
%    - response_names (equals row header of the data)
%    - data (rows x cols)
%    - target_responses (for a single data block; consistent over all data)
%    - theories
%        - name
%        - type
%        - numbers (means | criteria)
%
global THEORY_DICT

% The theory dictionary is ordered by the df a theory has - lower indices
% indicate theories with more df. This is used to hierarchically compare
% theories in the parsing of log data and therefore a requirement for any
% newly created theory type inserted here!
THEORY_DICT = {'free', 'pow', 'power', 'lin', 'linear', ...
    'abs', 'absolut', 'tot', 'total'};

fprintf("Loading data from %s...\n", filename);
[~, sheet_names] = xlsfinfo(filename);

% Test for the argument's type and ensure cell type
if isempty(sheet) % includes '', {}, []
    % Load all non-empty sheets
    sheets_to_load = sheet_names;
elseif iscell(sheet)
    % Load all specified sheets
    sheets_to_load = sheet;
elseif ischar(sheet)
    % Load exactly one sheet
    sheets_to_load = { sheet };
else
    error(['Invalid sheet argument! Must be either a single sheet '...
        'name, a cell containing multiple sheet names or an empty cell.'])
end

% Check for each sheet if it exists in the workbook (extra iteration so we 
% don't compute unnecessarily in case of an invalid sheet name)
for i = 1:length(sheets_to_load)
    cur = sheets_to_load{i};
    if ~any(ismember(sheet_names, cur))
        [~, name] = fileparts(filename);
        error('%s does not contain a sheet named %s!', name, cur);
    end
end

% Basic preallocation
%FIXME is there a more elegant way to preallocate?
data_struct(length(sheets_to_load)).name = '';
empty_sheets = [];

% Iterate over the sheets and sort the data into the allocated struct 
for i = 1:length(sheets_to_load)
    
    cur = char(sheets_to_load(i));
    [num, txt, cur_cell] = xlsread(filename, cur);
    
    if ~(isempty(num) || isempty(txt)) % skip empty sheets
        % Remove comment cells
        cur_cell(cellfun(@(c) ischar(c)&&startsWith(c,'%'), ...
            cur_cell)) = {NaN};
        % Remove empty rows
        cur_cell(all(cellfun(@(c) ~ischar(c)&&isnan(c),cur_cell),2),:) = [];
        % Remove empty cols (might be present due to comment removal)
        cur_cell(:,all(cellfun(@(c) ~ischar(c)&&isnan(c),cur_cell),1)) = [];
        
        row_headers = cur_cell(:,1);
        col_headers = cur_cell(1,2:end);
        % Remove trailing NaN values
        col_headers(cellfun(@(c) ~ischar(c)&&isnan(c),col_headers)) = [];
        
        % Identify stimulus name and target response headers
        name_idx = startsWith(row_headers, '!');
        name_cell = cellfun(@(c) lower(strtrim(c(2:end))), ...
            row_headers(name_idx),'un',0);
        
        % Find out if there's a "! Name" entry in the given sheet
        stim_name_idx = contains(name_cell, 'name');
        % Find out if there's a "! Target" entry in the given sheet
        target_resp_idx = contains(name_cell, 'target');
        
        if ~any(target_resp_idx)
            error(['Found no target responses in sheet %s! Target ' ...
                'responses are mandatory! If target answers are ' ...
                'present: Did you add a tag?'], cur);
        end

        % Check if stimulus names were given
        no_stim_header = ~(any(stim_name_idx));
        
        % Get response data (stored in row headers as "answer")
        response_idx = cellfun(@(c) startsWith(c, '"') ...
            && endsWith(c, '"'), row_headers);
        responses = row_headers(response_idx);

        respno = length(responses);
        stimno = length(col_headers);

        if no_stim_header
            % then we have target responses and not stimulus names in the
            % first row
            % Generate artificial stimulus names as S1, S2, ...
            stimulus_names = [repmat('S',stimno,1), ...
                           arrayfun(@int2str, transpose(1:stimno))];
            stimulus_names = cellstr(stimulus_names).';
            target_responses = col_headers;
        else % the column headers contain stimulus names
            stimulus_names = col_headers(1:stimno);
            target_responses = cur_cell(2,2:end);
            target_responses(cellfun(@(c) ~ischar(c)&&isnan(c), ...
                target_responses)) = [];
        end
        
        % Check if all target responses are legal responses
        unique_target_responses = unique(target_responses);
        if ~all(cellfun(@(c) ismember(c, responses), ...
                unique_target_responses))
            error(['There are target responses in %s that do not '...
                'match any response in the row headers.'], cur);
        end
        
        % Parse the theory lines
        theory_idx = find(startsWith(row_headers, '#'));
        theories = [];
        th_len = length(theory_idx);
        
        if any(theory_idx)
            theories = struct(...
                'name',     cell(1, th_len),...
                'type',     cell(1, th_len),...
                'numbers',  nan(1,  th_len));
            
            for k = 1:length(theory_idx)
                th_line = char(row_headers(theory_idx(k)));
                th_line = th_line(2:end); % get rid of '#'
                th_line = strsplit(strtrim(th_line), ';');
                if length(th_line) < 2
                    theories(k).name = ''; % gets handled below
                    theories(k).type = th_line{1};
                else
                    theories(k).name = th_line{1};
                    theories(k).type = th_line{2};
                end
                numbers = cell2mat(cur_cell(theory_idx(k),2:end));
                numbers(isnan(numbers)) = [];
                theories(k).numbers = numbers;
                theories(k) = check_theory_format(theories(k), ...
                    [respno stimno]);
            end
            
            % Check for duplicate theory names
            name_cell = {theories.name};
            [n,~,x] = unique(name_cell);
            freqs = hist(x,unique(x));
            dups = n(freqs>1);
            
            if ~(isempty(dups))
                error(['In %s there are multiple theories with the same '...
                    'name. This can be caused by multiple theories of the '...
                    'same type not having any name or simply multiple ones '...
                    'being called identically. This is not allowed. Names '...
                    'found were: ' sprintf('\n%s',dups{:})], ...
                    char(sheets_to_load(i)));
            end
            
        else
            warning(['No theory lines have been found in %s. This '...
                'means that no analyses will be performed for this '...
                'specific sheet. You should check with your data '...
                'file if this is unexpected.'], char(sheets_to_load(i)));
        end
        
        % Parse LR test specification lines
        tests = struct('more_df_name',{},'less_df_name',{});
        test_idx = find(startsWith(row_headers, '?'));
        num_tests = length(test_idx);
        if any(test_idx)
            tests = struct('more_df_name',cell(1,num_tests),...
                           'less_df_name',cell(1,num_tests));
                   
            for k = 1:length(test_idx)
                test_line = row_headers{test_idx(k)};
                test_line = test_line(2:end); % get rid of '?'
                test_line = strsplit(strtrim(test_line), ';');
                
                ordered_test_line = ...
                    check_and_order_test(test_line, theories);
                
                tests(k).more_df_name = ordered_test_line{1};
                tests(k).less_df_name = ordered_test_line{2};
            end
        end
        
        data = nan(respno, stimno);
        % Get row and coll inidices for the numerical data
        data_corner_idx = [ 3-no_stim_header, 2 ];
        data(:,:) = cell2mat(cur_cell( ...
            data_corner_idx(1):data_corner_idx(1) + respno-1, ...
            data_corner_idx(2):data_corner_idx(2) + stimno-1));

        data_struct(i).name = char(sheets_to_load(i));
        data_struct(i).data = data;
        if invertCols
            data_struct(i).data = fliplr(data_struct(i).data);
            data_struct(i).stimulus_names = fliplr(stimulus_names);
            data_struct(i).target_responses = fliplr(target_responses);
        else
            data_struct(i).stimulus_names = stimulus_names;
            data_struct(i).target_responses = target_responses;
        end
        if invertRows
            data_struct(i).data = flipud(data_struct(i).data);
            data_struct(i).response_names = flipud(responses);
        else
            data_struct(i).response_names = responses;
        end
        data_struct(i).theories = theories;
        data_struct(i).tests = tests;
    else
        empty_sheets = [empty_sheets i]; %#ok<AGROW>
        if ~isempty(sheet)
            % We found an empty sheet and it was explicitely specified to
            % be loaded, so this might be unexpected.
            warning(['%s does not contain any data despite being ' ...
                'explicitely specified to be loaded. It will be ' ...
                'excluded from the following analysis.'], cur);
        end
    end
end

% Delete empty data struct entries
if ~isempty(empty_sheets)
    empty_sheets = sort(empty_sheets, 'descend');
    for i = 1:length(empty_sheets)
        data_struct(empty_sheets(i)) = [];
    end
end

% Check for stimulus and response consistency over all sheets
consistent = 1;
for k = 1:(length(data_struct)-1)
    consistent = consistent && isequal(data_struct(k).stimulus_names, ...
                                       data_struct(k+1).stimulus_names);
    consistent = consistent && isequal(data_struct(k).response_names, ...
                                       data_struct(k+1).response_names);
    if (consistent == 0)
        error(['The data matrices in %s and %s differ in their '...
            'structure.'], data_struct(k).name, ...
            data_struct(k+1).name);
    end
end

end % fct

function theory = check_theory_format(theory, data_size)
% Take a theory and check if its numbers match the expected format for its
% type. Also check if it has a valid type and collapse longforms as
% 'linear' to their shorthands as 'lin'.
global THEORY_DICT

type = lower(theory.type);

if strcmp(theory.name,'')
    theory.name = type;
% else
%     name = lower(theory.name);
end
    
if ismember(type, THEORY_DICT)
    if ~strcmp(type, 'free')
        theory.type = type(1:3);
    end
else
    error(['Unrecognised theory type: %s in theory %s. Expected one ' ...
        'of:\n', sprintf('%s\n', THEORY_DICT{:})],theory.type,theory.name);
end

num_list = theory.numbers;
if strcmp(theory.type, 'tot')
    exp_length = data_size(2) + (data_size(1)-1);
elseif any(strcmp(theory.type, {'lin','pow','abs'}))
    exp_length = data_size(2);
elseif strcmp(theory.type, 'free')
    exp_length = 0;
end
if length(num_list) ~= exp_length
    error(['Theory %s has the wrong amount of parameters. Expected ' ...
        '%d, found %d.'], theory.name, exp_length, length(num_list));
end       
end

function ordered_test_line = check_and_order_test(test_line, theories)
% Take a cell array if theory names and check if there are exactly two
% names (as a LR test requires two models) and if the names match entries
% of the theories struct of the same data sheet.
% Returns a cell containing the two names ordered by their respective
% degrees of freedom (theory with more df comes first), so the LR test can 
% safely be performed.
global THEORY_DICT

if length(test_line) ~= 2
    error(['LR test specifications must contain exactly 2 entries. ' ...
        'Found %d'], length(test_line));
end

% Lowering all names for possible accidental capitalisation (MS Excel
% sometimes does that)
name1 = lower(test_line{1});
name2 = lower(test_line{2});
th_names = cellfun(@lower,{theories.name},'un',0);

if ~(any(strcmp(name1, th_names)) && any(strcmp(name2, th_names)))
    error(['Workbook contains a LR test with unknown theory names: ' ...
        '%s and %s'], name1, name2);
end

idx1 = strcmp(name1, th_names);
idx2 = strcmp(name2, th_names);
type1 = theories(idx1).type;
type2 = theories(idx2).type;
numbers1 = theories(idx1).numbers;
numbers2 = theories(idx2).numbers;

% Get the shorter length of the two number sets, so comparing theories with
% different parameter restrictions (e.g. means+crits vs. just crits) works
shorter_len = min(length(numbers1), length(numbers2));

if ~any(strcmp('free',{type1, type2})) ...
        && ~all(numbers1(1:shorter_len) == numbers2(1:shorter_len))
    error(['Test specification ''? %s;%s'' invalid. The theories ' ...
        'do not base on the same d'' values.'], name1, name2);
else
    rank1 = find(strcmp(type1(1:3), THEORY_DICT));
    rank2 = find(strcmp(type2(1:3), THEORY_DICT));
    
    if strcmp(type1, 'free')
        rank1 = 1;
    end
    if strcmp(type2, 'free')
        rank2 = 1;
    end
    
    % A lesser rank indicates more df (less restrictions)
    if rank1 < rank2 
        ordered_test_line = {name1, name2};
    elseif rank1 > rank2
        ordered_test_line = {name2, name1};
    else
        error(['It appears a test specification tries to compare ' ...
            'equivalent models: %s and %s. Their df do not differ ' ...
            'from one another.'], name1, name2);
    end
end
end
