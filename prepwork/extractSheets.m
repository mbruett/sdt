names = sheetnames("poisson_for_Malte.xlsx");

for i = 1:length(names)
    [~,~, data] = xlsread("poisson_for_Malte.xlsx", names(i));
    
    writecell(data,sprintf('%s.csv', names(i)));

end