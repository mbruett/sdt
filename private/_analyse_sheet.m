function analyse_sheet (struct_entry)
%
%   Analyse a single struct entry consisting of 1..n tables of data
%
num_tables = size(struct_entry.data, 3);

classification = zeros(size(struct_entry.data(:,:,1)));
for i = 1:length(struct_entry.response_names)
    for j = 1:length(struct_entry.response_names)
        classification(i,j) = strcmp(struct_entry.response_names(i), ...
                                     struct_entry.target_responses(j));
    end
end

for i = 1:num_tables
    %%% analyse single tables
end
%%% then analyse aggregated data

%%% print results to file

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% data = load_data(filename, '');
data_struct = get_data(filename, '');
% for k = 1:length(data_struct) % FIXME: extraction
%     data(:,:,k) = data_struct(k).data; %#ok<AGROW>
% end

% find the maximum number of tags given behind the identifier
[~, sheet_names] = xlsfinfo(filename);
max_conditions = 0;
for i = 1:length(sheet_names)
    tmp = sheet_names{i};
    tmp = strsplit(tmp, ';');
    num_conditions = (length(tmp)-1);
    if num_conditions > max_conditions
        max_conditions = num_conditions;
    end
end

num_criteria    = size(data_struct(1).data, 1) - 1;
num_means       = size(data_struct(1).data, 2);
num_param       = num_criteria + num_means;

% likelihood | criteria | 0 | further distribution means
parameters = zeros(length(sheet_names), num_param + 1);

for i = 1:length(sheet_names)
    fprintf('Analysing %s...\n', char(sheet_names(i)));
    [logli, params, crits] = fit(data_struct(i), addtrial);
    means = params(num_criteria+1:end);
    parameters(i,:) = [logli, crits, 0, means];
end

% Create file for tabular results
% TODO: Create folder structure to store results separately
tmp = strsplit(filename,'.');
resultFile = [char(tmp(1)), '_SDT_tab'];

% +1 for ID + max_conditions
result_cell = cell(size(parameters,1), ...
                   size(parameters,2) + 1 + max_conditions);

result_cell{1,1} = 'ID';
result_cell{1,2} = 'Trial';
for j = 1:max_conditions
    result_cell{1,j+2} = ['cond_' int2str(j)];
end
result_cell{1,max_conditions+3} = 'Log. Likelihood';
for j = 1:num_criteria
    result_cell{1,j+3+max_conditions} = ['c^' int2str(j) '_' int2str(j+1)];
end
for j = 1:num_means
    result_cell{1,j+3+max_conditions+num_criteria} = ['�_' int2str(j)];
end
% fill data
for i = 1:size(parameters,1)
    name_cell = strsplit(char(sheet_names(i)),';');
    for j = 1:(max_conditions+1)
        if length(name_cell) >= j
            result_cell{i+1,j} = char(name_cell(j));
        else
            result_cell{i+1,j} = '-';
        end
    end
    for j = 1:size(parameters,2)
        result_cell{i+1,j+2+max_conditions} = parameters(i,j);
    end
end

xlswrite(resultFile,result_cell,1);
fprintf('All results written to file %s.xls!\n', resultFile);

end % fct
