function data = load_data(filename, sheet)
%
%   LOAD_DATA( filename, sheet ) loads SDT data from a chosen xls file.
%
%   ARGUMENTS:
%   sheet   -   Specify a sheet to extract. If empty ('') all sheets are 
%               extracted.
%
%   RETURNS:
%   data    -   Vector of result tables.
%
%[file, path] = uigetfile({'*.xls*',"Excel-Dateien"});
%filename = [path file];
fprintf("Loading data from %s\n\n", filename);

[~, sheet_names] = xlsfinfo(filename);
%fprintf("Found sheets:\n"); 
%for i = 1:length(sheet_names)
%    fprintf("%s\n", char(sheet_names{i}));
%end
%fprintf('\n');

% try and look for the first non-empty sheet
for i = 1:length(sheet_names)
    [num, template] = xlsread(filename, i); % for comparison (see below)
    if not(isempty(num) && isempty(template))
        break;
    end
end

% Load a specified sheet if given and valid, load all otherwise
% TODO: support for lists of sheet names
if (isstring(sheet) || ischar(sheet)) && ~isempty(sheet) ...
        && any(ismember(sheet_names, sheet))
    sheet_names = {sheet};
else
    if ~isempty(sheet)
        error('Could not find specified sheet name!')
    end
end    

data = zeros(size(num,1),size(num,2),length(sheet_names));
% for i = 1:length(sheet_names)
%     [num, txt] = xlsread(filename, i);
%     if not(isempty(txt)) && isequal(txt, template) % ignore empty sheets
%         data(:,:,i) = num;
%     else
%         error(['Sheet does not match the workbook''s table structure!' ...
%             'Are the row and column headers identical for each sheet?'])
%     end
% end
sheet_names = char(sheet_names); % cast
for i = 1:length(sheet_names)
    [num, txt] = xlsread(filename, sheet_names(i,:));
    if not(isempty(txt)) % ignore empty sheets
        data(:,:,i) = num;
    else
        error(['Sheet appears to be empty!'])
    end
end

end % fct
