function [loli,hypo] = maxparamloglike(data,modelloglike,df)
% calculates log likelihood of maximum parameter model
if df > 0
    respno = size(data,1);
    stimno = size(data,2);
    total = sum(data,1);

    quota = nan(size(data));
    for r=1:respno
        quota(r,:) = data(r,:)./total;
    end

    loli = 0;
    for r=1:respno
        for s=1:stimno
           loli = loli + data(r,s) * log(quota(r,s));
        end
    end

    if modelloglike < 0
        testgroesse = -2*(modelloglike-loli);
        kritisch = chi2inv(.95,df);
        if testgroesse < kritisch % support H_0
            fprintf(['The LR test gives:\n\n' ...
                '-2 log LR = %.2f < %.2f = c, %d degrees of freedom' '\n\n' ...
                'Therefore the null hypothesis cannot be rejected. ' ...
                'The restricted model is\ncompatible with the data.\n'], ...
                testgroesse, kritisch, df);
                hypo = 'restricted';
        else % reject H_0
            fprintf(['The LR test gives:\n\n' ...
                '-2 log LR = %.2f > %.2f = c, %d degrees of freedom' '\n\n' ...
                'Therefore the null hypothesis has to be rejected. ' ...
                'The restricted model is not\ncompatible with the data.\n'], ...
                testgroesse, kritisch, df);
                hypo = 'unrestricted';
        end
    else
        fprintf(['Got a log. likelihood of %.2f < 0. No LR test will ' ...
        'be performed.\n'], modelloglike);
        loli = NaN;
        hypo = 'not applicable';
    end
else
    fprintf(['The restricted model does not differ from the unrestricted ' ...
        'model in terms of\ndegrees of freedom. A LR test will therefore ' ...
        'not be performed.\n']);
    loli = NaN;
    hypo = 'not applicable';
end

end % fct
