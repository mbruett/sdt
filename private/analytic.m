function [answer, criteria] = analytic( mu, pw, rel_stims )
%
%   ANALYTIC ( mu, pw, rel_stims ) finds a SDT model answer for the given
%   �'s and probability weights.
%   
%   ARGUMENTS:
%   mu          -   vector of the assumed distributions' means
%   pw          -   vector of the assumed probability weights of response
%                   categories
%   rel_stims   -   vector containing the relative frequencies of all
%                   stimuli respective to the total amount of observations
%
global data classification

respno = size(data,1);

classes(respno).mu = NaN;
classes(respno).n  = NaN;
criteria = nan(1,respno-1);

% match �'s and relative frequencies to the responses
for i = 1:length(classes)
    validclasses = classification(i,:) == 1;
    if sum(validclasses) == 0 % there is no stimulus to this response
        classes(i).mu = NaN;        % FIXME workaround
        classes(i).n  = 0;          % FIXME workaround
    else
        classes(i).mu = mu(validclasses); % choose all matching �'s
        classes(i).n  = rel_stims(validclasses);
    end

end
% fprintf('%f %f %f %f %f %f\n', mu);

%c12 = getcrit(mu(1),mu(2:end-1),pw(1),pw(2));
%c23 = getcrit(mu(2:end-1),mu(end),pw(2),pw(3));
for i = 1:(length(criteria)) % |criteria| = |classes|-1
    classes_left = classes(1:i);
    classes_right = classes(i+1:end);
    
    if classes_left(end).mu(end) == classes_right(1).mu(1)
        warning(['Found two stimulus classes with the same estimated ' ...
            'mean. As no decision criterion can be inferred from this ' ...
            'the mean itself will be given as the criterion.'])
        criteria(i) = classes_right(1).mu(1);
    else
        criteria(i) = getcrit([classes_left.mu], [classes_right.mu], ...
            pw(i),         pw(i+1), ...
            [classes_left.n], [classes_right.n]);
    end
end
% criteria = sort(criteria);
answer = get_modelanswer(mu, criteria);
end % fct
