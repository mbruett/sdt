% function [loli,pw,crit,means,modelanswer] = fit(data_matrix, answer_matrix)
function [logli, pw, crits, scalefac, exponent, means, rel_modelanswer] ...
    = fit(data_matrix, classification_matrix, theory_struct)
%
%   FIT( data_matrix, answer_matrix ) takes a response matrix and performs 
%   a SDT maximum likelihood analysis, returning the final logarithmic 
%   likelihood for the calculated model and the parameters thereof.
%
%   ARGUMENTS:
%   data_matrix     -   A matrix containing the test data to be analyzed. 
%                       This program expects the stimuli to be located in 
%                       the matrix' columns and the answers along the rows,
%                       headers stripped.
%   answer_matrix   -   A matrix of the same dimensions as the data
%                       containing just 1's and 0's signifying the mapping
%                       from answers to stimuli (to enable multiple stimuli
%                       being associated with the same answer).
%
%   RETURNS:
%   loli            -   The computed likelihood for the internal SDT model
%   pw              -   Probability weights of the stimuli's distributions
%   crit            -   Decision criteria between the model's responses
%   means           -   The model's stimulus distribution means
%   modelanswer     -   Matrix of the model's relative reponse frequencies
%                       based on the given fit. Multiply by the total
%                       absolute stimulus frequencies column-wise to get an
%                       analytic "most likely" model
%
global data mode mean_restriction classification %FIXME lessen globals?
tic;
% global d1 d2 dn1 dn2 wa wb p1 p2

data = data_matrix;
classification = classification_matrix;
mode = theory_struct.type;
mean_restriction = theory_struct.numbers; % [] for 'free', unused for 'tot'

respno = size(data,1); % rows for responses
stimno = size(data,2); % columns for stimuli

% starting values for parameters
%TODO add in getstartparam.m
[pw, means] = getstart(data);
%means = (1:(stimno-1)); % first mean is set to zero, therefore excluded
%pw = ones(1, (respno-1));

scalefac = means(end);
%scalefac = 1;
exponent = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
    case 'free' % full optimisation
        startparam = [pw means];
        param = fminsearch(@liho,startparam,optimset('Display','off'));
        [logli, crits, scalefac, exponent, means, rel_modelanswer] = liho(param);
        pw = param(1:respno-1);
        logli = -logli;
    case 'linear' % just scale factor and criteria to be optimised
        startparam = [pw scalefac];
        param = fminsearch(@liho,startparam,optimset('Display','off'));
        [logli, crits, scalefac, exponent, means, rel_modelanswer] = liho(param);
        pw = param(1:respno-1);
        logli = -logli;
    case 'power' % scale factor, exponent and criteria to be optimised
        startparam = [pw scalefac exponent];
        param = fminsearch(@liho,startparam,optimset('Display','off'));
        [logli, crits, scalefac, exponent, means, rel_modelanswer] = liho(param);
        pw = param(1:respno-1);
        logli = -logli;
    case 'absolut' % means fixed, crit optim only
        startparam = pw;
        param = fminsearch(@liho,startparam,optimset('Display','off'));
        [logli, crits, scalefac, exponent, means, rel_modelanswer] = liho(param);
        pw = param;
%         means = mean_restriction;
        logli = -logli;
    case 'total' % total theories don't require optimisation
        means = theory_struct.numbers(1:stimno);
        crits = theory_struct.numbers(stimno+1:end);
        rel_modelanswer = get_modelanswer(means, crits);
        logli = get_model_logli(data, rel_modelanswer);
        pw = nan(1,length(crits)); %FIXME
    otherwise
        error('Unrecognised theory type: %s', mode);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% result logging
fprintf([repelem('-',80) '\n']);
fprintf('Analysis finished (%.2f s). Found:\nlog. likelihood:\t%f\n', ...
    toc, logli);
fprintf('pw:\t\t\t\t\t'); fprintf(' %f',pw); fprintf('\n');
fprintf('c:\t\t\t\t\t'); fprintf(' %f',crits); fprintf('\n');
fprintf('�:\t\t\t\t\t'); fprintf(' %f',means); fprintf('\n');
fprintf('scale factor:\t\t'); fprintf(' %f',scalefac); fprintf('\n');
fprintf('exponent:\t\t\t'); fprintf(' %f',exponent); fprintf('\n');

end % fct
