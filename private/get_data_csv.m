function data_struct = get_data_csv(base, set)

global THEORY_DICT

% The theory dictionary is ordered by the df a theory has - lower indices
% indicate theories with more df. This is used to hierarchically compare
% theories in the parsing of log data and therefore a requirement for any
% newly created theory type inserted here!
THEORY_DICT = {'free', 'power', 'linear', 'absolut', 'total', 'gauss', 'poisson'};

% Create dialog window for data processing.
pleaseHold = waitbar(0,'Loading data from file. Please wait...');

% Set directory to get data from.
dataDirectory = [base.home filesep 'projects' filesep base.projectName filesep 'data'];

fprintf("Loading data from %s...\n", [dataDirectory filesep base.dataFile]);

% Get archive name without extension.
[~, nameOnly,~] = fileparts(base.dataFile);
% Get set names.
set_names = split(unzip([dataDirectory filesep base.dataFile], [dataDirectory filesep nameOnly]), '.csv');
if length(size(set_names)) > 2
    set_names = set_names(:,:,1);
else
    set_names = set_names(1,:);
end

% Test for the argument's type and ensure cell type
if isempty(set) % includes '', {}, []
    % Load all non-empty sets
    sets_to_load = set_names;
elseif iscell(set)
    % Load all specified sets
    sets_to_load = set;
elseif ischar(set)
    % Load exactly one set
    sets_to_load = { set };
else
    error(['Invalid set argument! Must be either a single set '...
        'name, a cell containing multiple set names or an empty cell.'])
end

% Check for each set if it exists in the workbook (extra iteration so we 
% don't compute unnecessarily in case of an invalid set name)
for i = 1:length(sets_to_load)
    cur = sets_to_load{i};
    if ~any(ismember(set_names, cur))
        error('%s does not contain a set named %s!', name, cur);
    end
end

% Basic preallocation
%FIXME is there a more elegant way to preallocate?
data_struct(length(sets_to_load)).name = '';
empty_sets = [];

% Update waitbar.
waitbar(.5,pleaseHold,'Loading data from file. Please wait...');

% Iterate over the sets and sort the data into the allocated struct 
for i = 1:length(sets_to_load)
    %% Set preparation.
    cur = char(sets_to_load(i));
    cur_cell = readcell([cur '.csv'], 'CommentStyle','%');

    dataCheck = cur_cell(3:end, 2:end);

    if ~isempty(dataCheck) % skip empty sets
        % Remove comment cells
        cur_cell(cellfun(@(c) ischar(c)&&startsWith(c,'%'), ...
            cur_cell)) = {NaN};
        % Remove empty rows
        cur_cell(cellfun(@(c) ~ischar(c)&&ismissing(c),cur_cell)) = {NaN};
        cur_cell(all(cellfun(@(c) ~ischar(c)&&isnan(c),cur_cell),2),:) = [];
        % Remove empty cols (might be present due to comment removal)
        cur_cell(:,all(cellfun(@(c) ~ischar(c)&&isnan(c),cur_cell),1)) = [];

        % Get headers.
        row_headers = cur_cell(:,1);
        col_headers = cur_cell(1,2:end);
        

        %% Response headers
        % Identify stimulus name and target response headers
        name_idx = startsWith(row_headers, '!');
        name_cell = cellfun(@(c) lower(strtrim(c(2:end))), ...
            row_headers(name_idx),'un',0);
        
        % Find out if there's a "! Name" entry in the given set
        stim_name_idx = contains(name_cell, 'name');
        % Find out if there's a "! Target" entry in the given set
        target_resp_idx = contains(name_cell, 'target');
        if ~any(target_resp_idx)
            error(['Found no target responses in set %s! Target ' ...
                'responses are mandatory! If target answers are ' ...
                'present: Did you add a tag?'], cur);
        end

        % Get response data (stored in row headers as "answer")
        response_idx = cellfun(@(c) startsWith(c, '"'), row_headers);
        respHeaders = row_headers(response_idx);

        responseNumber = length(respHeaders);     % number of possible answers
        stimulusNumber = length(col_headers);   % number of different stim categories

        % Check if stimulus names were given
        no_stim_header = ~(any(stim_name_idx));
        if no_stim_header
            % then we have target responses and not stimulus names in the
            % first row
            % Generate artificial stimulus names as S1, S2, ...
            stimulus_names = [repmat('S',stimulusNumber,1), ...
                           arrayfun(@int2str, transpose(1:stimulusNumber))];
            stimulus_names = cellstr(stimulus_names).';
            target_responses = col_headers;
        else % the column headers contain stimulus names
            stimulus_names = col_headers(1:stimulusNumber);
            target_responses = cur_cell(2,2:end);
            target_responses(cellfun(@(c) ~ischar(c)&&isnan(c), ...
                target_responses)) = [];
        end
        
        % Check if all target responses are legal responses
        unique_target_responses = unique(target_responses);
        if ~all(cellfun(@(c) ismember(c, respHeaders), ...
                unique_target_responses))
            error(['There are target responses in %s that do not '...
                'match any response in the row headers.'], cur);
        end
       
        %% Theory lines
        theory_idx = find(startsWith(row_headers, '#')); % indices for theory rows
        theories = []; % array to save theory info to
        th_len = length(theory_idx); % number of theories
        
        if any(theory_idx) % if there are theories listed
            theories = struct(...
                'name',     cell(1, th_len),...
                'type',     cell(1, th_len),...
                'numbers',  nan(1,  th_len));
            
            for k = 1:th_len
                th_line = char(row_headers(theory_idx(k)));
                th_line = th_line(2:end); % get rid of '#'
                th_line = strsplit(strtrim(th_line), ';');
                if length(th_line) < 2
                    theories(k).name = ''; % gets handled below
                    theories(k).type = th_line{1};
                else
                    theories(k).name = th_line{1};
                    theories(k).type = th_line{2};
                end
                numbers = cell2mat(cur_cell(theory_idx(k),2:end));
                numbers(isnan(numbers)) = []; % i.e. gauss
                theories(k).numbers = numbers;
                theories(k) = check_theory_format(theories(k), ...
                    [responseNumber stimulusNumber]);
            end
            
            % Check for duplicate theory names
            name_cell = {theories.name};
            [n,~,x] = unique(name_cell);
            freqs = hist(x,unique(x));
            dups = n(freqs>1);
            
            if ~(isempty(dups))
                error(['In %s there are multiple theories with the same '...
                    'name. This can be caused by multiple theories of the '...
                    'same type not having any name or simply multiple ones '...
                    'being called identically. This is not allowed. Names '...
                    'found were: ' sprintf('\n%s',dups{:})], ...
                    char(sets_to_load(i)));
            end
            
        else
            warning(['No theory lines have been found in %s. This '...
                'means that no analyses will be performed for this '...
                'specific set. You should check with your data '...
                'file if this is unexpected.'], char(sets_to_load(i)));
        end
        

        %% LR test specification
        tests = struct('more_df_name',{},'less_df_name',{});
        test_idx = find(startsWith(row_headers, '?'));
        num_tests = length(test_idx);
        if any(test_idx)
            tests = struct('more_df_name',cell(1,num_tests),...
                           'less_df_name',cell(1,num_tests));
                   
            for k = 1:length(test_idx)
                test_line = row_headers{test_idx(k)};
                test_line = test_line(2:end); % get rid of '?'
                test_line = strsplit(strtrim(test_line), ';');
                
                ordered_test_line = ...
                    check_and_order_test(test_line, theories);
                
                tests(k).more_df_name = ordered_test_line{1};
                tests(k).less_df_name = ordered_test_line{2};
            end
        end
        

        %% Response numbers
        dataDirectory = nan(responseNumber, stimulusNumber);
        % Get row and coll inidices for the numerical data
        data_corner_idx = [ 3-no_stim_header, 2 ];
        dataDirectory(:,:) = cell2mat(cur_cell( ...
            data_corner_idx(1):data_corner_idx(1) + responseNumber-1, ...
            data_corner_idx(2):data_corner_idx(2) + stimulusNumber-1));

        data_struct(i).name = char(sets_to_load(i));
        data_struct(i).data = dataDirectory;
        data_struct(i).stimulus_names = stimulus_names;
        data_struct(i).target_responses = target_responses;
        data_struct(i).response_names = respHeaders;
        data_struct(i).theories = theories;
        data_struct(i).tests = tests;
    else
        empty_sets = [empty_sets i]; %#ok<AGROW>
        if ~isempty(set)
            % We found an empty set and it was explicitely specified to
            % be loaded, so this might be unexpected.
            warning(['%s does not contain any data despite being ' ...
                'explicitely specified to be loaded. It will be ' ...
                'excluded from the following analysis.'], cur);
        end
    end
end
% Delete empty data struct entries
if ~isempty(empty_sets)
    empty_sets = sort(empty_sets, 'descend');
    for i = 1:length(empty_sets)
        data_struct(empty_sets(i)) = [];
    end
end

% Check for stimulus and response consistency over all sheets
consistent = 1;
for k = 1:(length(data_struct)-1)
    consistent = consistent && isequal(data_struct(k).stimulus_names, ...
                                       data_struct(k+1).stimulus_names);
    consistent = consistent && isequal(data_struct(k).response_names, ...
                                       data_struct(k+1).response_names);
    if (consistent == 0)
        error(['The data matrices in %s and %s differ in their '...
            'structure.'], data_struct(k).name, ...
            data_struct(k+1).name);
    end
end

% FIXME: save data to file
cd(fullfile(base.home, "projects", base.projectName, "data"));
fn = sprintf('%s.mat', string(nameOnly));
save(fn,"data_struct");
cd(base.home);

% Update and close waitbar.
waitbar(1,pleaseHold,'Data fully loaded.');
close(pleaseHold)
end

function theory = check_theory_format(theory, data_size)
% Take a theory and check if its numbers match the expected format for its
% type. Also check if it has a valid type and collapse longforms as
% 'linear' to their shorthands as 'lin'.
global THEORY_DICT

type = lower(theory.type);

if strcmp(theory.name,'')
    theory.name = type;
% else
%     name = lower(theory.name);
end

if ismember(type, THEORY_DICT)
        theory.type = type;
else
    error(['Unrecognised theory type: %s in theory %s. Expected one ' ...
        'of:\n', sprintf('%s\n', THEORY_DICT{:})],theory.type,theory.name);
end
   
num_list = theory.numbers;
if strcmp(theory.type, 'total')
    exp_length = data_size(2) + (data_size(1)-1);
elseif any(strcmp(theory.type, {'linear','power','absolut'}))
    exp_length = data_size(2);
elseif any(strcmp(theory.type, {'free', 'gauss','poisson'}))
    exp_length = 0;
end
if length(num_list) ~= exp_length
    error(['Theory %s has the wrong amount of parameters. Expected ' ...
        '%d, found %d.'], theory.name, exp_length, length(num_list));
end       
end

function ordered_test_line = check_and_order_test(test_line, theories)
% Take a cell array of theory names and check if there are exactly two
% names (as a LR test requires two models) and if the names match entries
% of the theories struct of the same data sheet.
% Returns a cell containing the two names ordered by their respective
% degrees of freedom (theory with more df comes first), so the LR test can 
% safely be performed.
global THEORY_DICT

if length(test_line) ~= 2
    error(['LR test specifications must contain exactly 2 entries. ' ...
        'Found %d'], length(test_line));
end

% Lowering all names for possible accidental capitalisation (MS Excel
% sometimes does that)
name1 = lower(test_line{1});
name2 = lower(test_line{2});
th_names = cellfun(@lower,{theories.name},'un',0);

if ~(any(strcmp(name1, th_names)) && any(strcmp(name2, th_names)))
    error(['File contains a LR test with unknown theory names: ' ...
        '%s and %s'], name1, name2);
end

idx1 = strcmp(name1, th_names);
idx2 = strcmp(name2, th_names);
type1 = theories(idx1).type;
type2 = theories(idx2).type;
numbers1 = theories(idx1).numbers;
numbers2 = theories(idx2).numbers;

% Get the shorter length of the two number sets, so comparing theories with
% different parameter restrictions (e.g. means+crits vs. just crits) works
shorter_len = min(length(numbers1), length(numbers2));

if ~any(strcmp({'free', 'gauss'},{type1, type2})) ...
        && ~all(numbers1(1:shorter_len) == numbers2(1:shorter_len))
    error(['Test specification ''? %s;%s'' invalid. The theories ' ...
        'do not base on the same d'' values.'], name1, name2);
else
    rank1 = find(strcmp(type1, THEORY_DICT));
    rank2 = find(strcmp(type2, THEORY_DICT));
    
    if strcmp(type1, 'free')
        rank1 = 1;
    end
    if strcmp(type2, 'free')
        rank2 = 1;
    end
    
    % A lesser rank indicates more df (less restrictions)
    if rank1 < rank2 
        ordered_test_line = {name1, name2};
    elseif rank1 > rank2
        ordered_test_line = {name2, name1};
    else
        error(['It appears a test specification tries to compare ' ...
            'equivalent models: %s and %s. Their df do not differ ' ...
            'from one another.'], name1, name2);
    end
end
end
