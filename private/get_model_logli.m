function logli = get_model_logli(data, relative_modelanswer)
%
respno = size(data,1);
stimno = size(data,2);
logli = 0;
for i = 1:respno
    for j = 1:stimno
        logli = logli + data(i,j) * log(relative_modelanswer(i,j));
    end
end

end % fct
