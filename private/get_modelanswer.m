function rel_modelanswer = get_modelanswer(means, criteria)
%
respno = length(criteria) + 1;
stimno = length(means);

rel_modelanswer = zeros(respno, stimno);

for j = 1:stimno
    cumulative_answers = 0;
    for i = 1:(respno-1)
        rel_modelanswer(i,j) = normcdf(criteria(i),means(j),1);
        rel_modelanswer(i,j) = rel_modelanswer(i,j) - cumulative_answers;
        % answers have to be lower bound to zero, as no negative
        % frequencies are possible and non-real loglis would be computed
        rel_modelanswer(i,j) = max(0, rel_modelanswer(i,j));
        cumulative_answers = cumulative_answers + rel_modelanswer(i,j);
    end
    rel_modelanswer(respno,j) = 1 - cumulative_answers;
end

end % fct
