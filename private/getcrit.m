function crit = getcrit(dis1,dis2,a,b,n1,n2)

global d1 d2 dn1 dn2 wa wb
d1 = dis1;
d2 = dis2;
dn1 = n1;
dn2 = n2;
wa = a;
wb = b;

crit = fzero(@lihora_minus_one,0,optimset('Display','off'));
end % fct
