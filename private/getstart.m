function [likelihoodratios, mu] = getstart(data)

answers = data;
respno = size(answers,1);
stimno = size(answers,2);  %% not needed

% get first values of startparam: 
% likelihood ratios (per default all equal to 1)
likelihoodratios = ones(1,respno-1);

% now to the other parameters... 
% the means of the distributions representing the stimuli

% first of all: get relative answers, i.e. get rid of total number
totalanswers = sum(answers,1);
for s=1:stimno
    answers(:,s) = answers(:,s) / totalanswers(s);
end

% now determine "mean answer per stimulus"
% set up weights: answer 1 weights 1, answer 2 weights 2...
weights = nan(size(answers));
for r=1:respno
    weights(r,:)=r;
end
% multiply with answers and get mean = mean answer per stimulus
wanswers = answers .* weights;
meanwanswers = mean(wanswers,1);

% get mu from mean answers:
% centered answers are centered around zero
centeredanswers = meanwanswers - mean(meanwanswers);

% that would already be a nice guess for the mu... but the variation might
% be to small... so divide by the variation to get mu that are around +-1

% get rms (root of mean of square) from centeredanswers
rms_centeredanswers = sqrt(mean(centeredanswers.*centeredanswers));

% get mu: centeredanswers divided by their rms (a measure of variation)
mu = centeredanswers/rms_centeredanswers;

% replace NaN (can be caused due to little variance in data)
if any(isnan(mu))
    warning(['Found stimulus categories with little variance. Analysis '...
        'results might be artificial.']);
    mu(isnan(mu)) = 0;
end

% by convention, mu(1)=0
% mu = mu - mu(1);
mu = mu - min(mu);
mu = sort(mu);
mu = mu(2:end);

% compose startparam: first likelihood ratios, then mu
% startparam = [likelihoodratios mu(2:end)];
