function [logli, crits, scalefac, expo, means, rel_modelanswer] = liho(param)
%
%   LIHO( param ) models the likelihood optimization process and is
%   called via MATLAB's fminsearch function to be optimized.
%   (fminsearch optimises the first function's output)
%
global data mode mean_restriction

respno = size(data,1); % rows
stimno = size(data,2); % columns

scalefac = 1;   % Reserve: nur falls nirgends anders gesetzt
expo = 1;       % dito

abs_stims = sum(data,1); % "no answer" has to be excluded at this point!
rel_stims = abs_stims/sum(abs_stims);

switch mode
    case 'free' % full optimisation
        if length(param) == (respno-1 + stimno-1)
            means = [0 param(respno:end)];
        else
            error(['For full optimisation (''free'') liho.m requires ' ...
                '(respno-1) + (stimno-1) parameters. Got %d instead.'],...
                length(param));
        end
    case 'linear' % just scale factor and criteria to be optimised
        if length(param) == (respno-1)+1 % for explicitness
            scalefac = param(respno);
            % restriction made here for consistency with "pow" case (see
            % below)
            scalefac = max(realmin,scalefac);
            means = mean_restriction * scalefac;
        else
            error(['For linearly restricted optimisation liho.m ' ...
                'requires (respno-1) + 1 parameters. Got %d instead.'],...
                length(param));
        end
    case 'power' % scale factor, exponent and criteria to be optimised
        if length(param) == (respno-1)+2 % for explicitness
            scalefac = param(respno);
            % avoid negative scalefacs => potentially complex means
            scalefac = max(realmin,scalefac);
            expo = param(end);
            expo = max(realmin,expo);
            %means = [0 (mean_restriction(2:end) * scalefac).^(expo)];
            means = [0 (mean_restriction(2:end).^(expo))* scalefac];
%             fprintf([sprintf('%d, ',means) '\n'])
        else
            error(['For power function restricted optimisation liho.m ' ...
                'requires (respno-1)+2 parameters. Got %d instead.'], ...
                length(param));
        end
    case 'absolut' % means fixed, crit optim only
        if length(param) == respno-1
            means = mean_restriction;
        else
            error(['For criteria only optimisation liho.m requires ' ...
                '(respno-1) parameters. Got %d instead.'], ...
                length(param));
        end
end

probweights = param(1:respno-1);

% = [1 1*pw21 1*pw21*pw32 ...]
pw = ones(1,length(probweights)+1);
for i = 1:length(probweights)
    pw(i+1) = pw(i)*probweights(i);
end
if any(~isreal(means))
    error('Some or all of the means are complex!')
end
[rel_modelanswer, crits] = analytic(means,pw,rel_stims);
% minma = min(min(rel_modelanswer));
% fprintf('%f %f %f ... %f %f ... %f\n',pw,crits,minma);

logli = 0;
for t = 1:respno
    for mun = 1:stimno
        logli = logli + data(t,mun) * log(rel_modelanswer(t,mun));
    end
end
% disp(rel_modelanswer);
% fprintf('> Optimization at log. likelihood =\t %1.20f\n', logli);
if ~isreal(logli)
    error('imaginary part of logli');
end
logli = -logli; % we look for a maximum, but logli is to be minimised
end % fct
