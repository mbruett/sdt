function lira = lihora_minus_one(x)

% calculates weighted likelihood ratio minus one
% finding the zero means: find x where
% (p2*wb)/(p1*wa) - 1 = 0
% (p2*wb)/(p1*wa) = 1
% p2/p1 = wa/wb

% FIXME: abstract into function

global d1 d2 dn1 dn2 wa wb p1 p2
% d = list of distribution means
% dn = list of distibution relative frequencies (ordered per mean)
% w = probability weights of the distribution classes

%fprintf('Testing\t %1.2f\n',x);

p1 = 0;
for i = 1:length(d1)
    if ~(isnan(d1(i))) % assumes dn(i)==0 else
        p1 = p1 + dn1(i)*normpdf(x,d1(i),1);
    end
end

p2 = 0;
for i = 1:length(d2)
    if ~(isnan(d2(i))) % assumes dn(i)==0 else
        p2 = p2 + dn2(i)*normpdf(x,d2(i),1);
    end
end

% FIXME workaround for non-real guesses that lead to a non-defined model
% with empty parameters
if p1 == 0
    p1 = realmin;
end

if wa == 0
    wa = realmin;
end
lira = (p2*wb)/(p1*wa) - 1;
% fprintf('wa: %d |wb: %d |p1: %d |p2: %d\nx: %d |lira: %d\n',wa,wb,p1,p2,x,lira);
end % fct
