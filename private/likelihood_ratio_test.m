function result = likelihood_ratio_test(logli1, logli2, df1, df2, h0, h1)
% Generic asymptotic Likelihood Ratio Test computation based on the 
% approximation of Wilks� theorem. Compares two models based on their
% (logarithmic) likelihood and degrees of freedom and returns a preference
% based on a chi^2 critical value.
%
% Assumes that the first model has less df, is therefore more restricted
% and corresponds to the H_0 of the LR test.

ddf = df2 - df1;
if ddf > 0
    if (logli1 < 0) && (logli2 < 0)
        test_statistic = -2*(logli1-logli2);
        critical_value = chi2inv(.95,ddf);
        if test_statistic < critical_value % support H_0
            fprintf(['The LR test gives:\n\n' ...
                '-2 log LR = %.2f < %.2f = c, %d degrees of freedom' '\n\n' ...
                'Therefore the null hypothesis cannot be rejected.\n' ...
                'The restricted model is compatible with the data.\n'], ...
                test_statistic, critical_value, ddf);
            result = h0;
        else % reject H_0
            fprintf(['The LR test gives:\n\n' ...
                '-2 log LR = %.2f > %.2f = c, %d degrees of freedom' '\n\n' ...
                'Therefore the null hypothesis has to be rejected.\n' ...
                'The restricted model is not compatible with the data.\n'], ...
                test_statistic, critical_value, ddf);
            result = h1;
        end
    else
        fprintf(['Got a invalid log. likelihood (got %.2f and %.2f). '...
            'No LR test will be performed.\n'], logli1, logli2);
        result = 'not applicable';
    end
else
    fprintf(['The restricted model does not differ from the unrestricted ' ...
        'model in terms of\ndegrees of freedom.\nA LR test will therefore ' ...
        'not be performed.\n']);
    result = 'not applicable';
end

end % fct
