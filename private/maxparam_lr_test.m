function [logli_maxparam, ddf, result] ...
    = maxparam_lr_test(data, logli_model, df_model)
% Compute the maximum parameter model for given data and perform a
% likelihood ration test against a given model's likelihood and df.

respno = size(data,1);
stimno = size(data,2);

df_maxparam = (respno - 1) * stimno;
ddf = df_maxparam - df_model;

if ddf > 0
    logli_maxparam = get_logli_maxparam(data);
    
    result = likelihood_ratio_test(logli_model, logli_maxparam, ...
        df_model, df_maxparam, 'restricted', 'unrestricted');
else
    fprintf(['The restricted model does not differ from the ' ...
        'unrestricted model in terms of\ndegrees of freedom.\nA LR test '...
        'will therefore not be performed.\n']);
    logli_maxparam = NaN;
    result = 'not applicable';
end

end % fct

function logli = get_logli_maxparam(data)
% Compute the maximum parameter model for a given data matrix and return
% its logarithmic likelihood.
logli = 0;
respno = size(data,1);
stimno = size(data,2);
total = sum(data,1);

quota = nan(size(data));
for r = 1:respno
    quota(r,:) = data(r,:)./total;
end
for r=1:respno
    for s=1:stimno
        logli = logli + data(r,s) * log(quota(r,s));
    end
end
end % fct
