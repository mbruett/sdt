function [name] = nameFile(name)
%% nameFile
%  Overwrites the input file name if the chosen name already exists.
%  It appends a number suffix depending on how many identical file there
%  are. (e.g. file, file(1), ...).
%
%   name     name of the file to save
%
%  RETURN:  adjusted name of file
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Checking for matching names
% Check if a folder of the same name exists.
check = exist(name, "file");
if check == 2 % name is a file
    suffix = 0;
    % Check again if file exists.
    check  = exist(name, "file");
    while check == 2 % name is a file
        % Set new name for file with a number suffix.
        name = sprintf('error-log(%d).txt',  suffix+1);
        suffix = suffix+1;
        % Check if file wirh that number suffix exists.
        check  = exist(name, "file");
    end
end
end