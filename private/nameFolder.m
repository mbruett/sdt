function [name] = nameFolder(name, type)
%% nameFolder
%  Overwrites the input folder name if the chosen name already exists.
%  It appends a number suffix depending on how many identical folders there
%  are. (e.g. folder, folder(1), ...).
%
%   name     name of the folder to save to
%   type     either 'images' or nothing for project folder
%
%  RETURN:  adjusted name of folder
%  NOTES:   nameFolder is used for two things: Setting the project name and
%           saving the generated images to their respective folders.
%           While the project name is set up directly in the working
%           directory, the images are saved in a subdirectory that needs to
%           be accessed to ensure proper processing. Hence the second
%           variable in the function call to distinguish both cases.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% directory based on where the function call comes from
baseDirectory = pwd;
%% Setting relevant directory
% If the data to be saved contains images the directory has to be changed
% to a subdirectory of the project folder.
if nargin == 2
    folderParentDirectory =  fullfile(baseDirectory,type);
else
    folderParentDirectory = baseDirectory;
end

%% Checking for matching names
% Check if a folder of the same name exists.
cd(folderParentDirectory);
check = exist(name, "dir");
if check == 7                     % name is a folder
    % dialog box for choosing to change the name or overwrite it
    writeMenu = ...
        questdlg(sprintf(['Seems like the folder "%s" already exists. ' ...
        'Do you want to overwrite the existing folder' ...
        ' contents?'], name), ...
        'CAUTION', 'Yes', 'No', 'No');
    switch writeMenu
        case 'Yes'  % Overwrite the existing folder.
            rmdir(name, 's');
            mkdir(name);
        case 'No'   % Create a new folder with an altered name.
            suffix = 0;
            last   = name;
            % Check again if folder exists.
            check  = exist(name, "dir");
            while check == 7 % yes
                % Set that name as the last one to exist
                name = last;
                % Set new name for folder with a number suffix.
                name = sprintf('%s%s%d%s', name, '(',  suffix+1, ')');
                suffix = suffix+1;
                % Check if folder wirh that number suffix exists.
                check  = exist(name, "dir");
            end
            % Create folder with suffixed name.
            mkdir(name);
    end
else
        % Create folder.
        mkdir(name)
end
% Go back to relevant directory.
cd(baseDirectory);
end