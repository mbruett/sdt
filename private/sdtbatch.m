function sdtbatch (dataStruct, base, addTrial, plot_switch, tabWrite, poissMu)

%% SDTBATCH (dataStruct, base ) performs SDT analyses on separate data sets. 
%
% ARGUMENTS:
%   dataStruct  - structure array containg the data sets
%   base        - structure array containing base info about this run  
%   options     - name value pairs for optional parameters
%
% OPTIONS:
% "addTrial", n             -  integer number of trials to "randomly"
%                              distribute to the data's cells. Common fix for data
%                              containing zero entries. Add n trials per stimulus.
%                              This leads to n/r trials per cell, where r is the
%                              number of possible reponses.
% "plotSwitch", "on"/"off"  -  Determines, whether the calculated models shall be plotted to ./plot/
% "tabWrite", "on"/"off"    -  Determines, whether the results shall be written to ./results/
% "poissMu", n              -  mean for the noise distribution of the Poisson model
% "invertCols", "on"/"off"  -  determines wether the columns of the data set should be inverted
% "invertRows", "on"/"off"  -  determines wether the rows of the data set should be inverted
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Prepare for analysis.
% Amount variables.
responseNumber       = size(dataStruct(1).data, 1);          % number of possible answers
criteriumNumber      = size(dataStruct(1).data, 1) - 1;      % number of criteria
meanNumber           = size(dataStruct(1).data, 2);          % number of means necessary
analysisNumber       = length([dataStruct.theories]);        % number of analyses to be done                            %+ length(data_struct);
theoryNumber         = analysisNumber == length(dataStruct);   % determine if there are theories

% result containers for theory tests
analysisNames       = cell(analysisNumber, 2); % set name + theory name

restrModelLLH       = nan(analysisNumber, 1);
criteria            = nan(analysisNumber, criteriumNumber);
means               = nan(analysisNumber, meanNumber);
scalefacs           = nan(analysisNumber, 1);
exps                = nan(analysisNumber, 1);

modelAnswers        = cell(responseNumber+1, meanNumber+1, analysisNumber);

dfArray             = nan(1, analysisNumber);
maxParamLLH         = nan(1, analysisNumber);
hyp_cell            = cell(1, analysisNumber);

analysisIdx = 1;

% result containers for LR tests
testNumber  = sum(arrayfun(@(e) length(e.tests), dataStruct));
LRnames     = cell(testNumber,3);
LRdfs       = nan(testNumber,1);
LRllh1      = nan(testNumber,1);
LTllh2      = nan(testNumber,1);
LRresults   = cell(testNumber,1);
testIdx     = 1;

%% Analyse each data set. [so much documentation missing, please add comments whenever possible]
%FIXME abstract!

pleaseHold = waitbar(0, {sprintf('Analysing set 0/%d', length(dataStruct))});
for i = 1:length(dataStruct) %for each set
    fprintf([repelem('=',80) '\n']);
    fprintf('Analysing sheet "%s"...\n', char(dataStruct(i).name));

    % [what does this do?]
    classification = find_classification(dataStruct(i));

    % Add trial(s) as a correction
    corrected_data      = add_trial(addTrial, dataStruct(i).data);
    uncorrected_data    = dataStruct(i).data(:,:,1); % named for clarity

    for m = 1:(length(dataStruct(i).theories)) %FIXME exlude
        %%% DEPRECATED: user has to specify even free
        %         if m == 1 % add 'atheoretical' run for free fitting
        %             cur_th = struct('name','free','type','fre','numbers',[]);
        %         else
        %             cur_th = data_struct(i).theories(m-1);
        %         end
        cur_th = dataStruct(i).theories(m);
        fprintf([repelem('-',80) '\n']);
        fprintf('Theory: "%s" (%s type)\n', ...
            char(cur_th.name), char(cur_th.type));

        % Fit model
        [model_logli, model_pw, model_criteria, model_scalefac, ...
            model_exponent, model_means, model_relative_answer] ...
            = fit(corrected_data, classification, cur_th);

        restrModelLLH(analysisIdx)  = model_logli;
        criteria(analysisIdx,:)     = model_criteria;
        means(analysisIdx,:)        = model_means;
        scalefacs(analysisIdx)      = model_scalefac;
        exps(analysisIdx)           = model_exponent;

        %TODO abstract!
        data_sums = repmat(sum(uncorrected_data,1),...
            size(uncorrected_data,1),1);
        absolute_modelanswer = model_relative_answer.*data_sums;
        if ~all(all((absolute_modelanswer == 0)))
            fprintf(['Euclidean distance between model answer and data:'...
                ' %.3f\n'], ...
                sqrt(sum(sum((corrected_data - absolute_modelanswer).^2))));
        end

        modelAnswers(1,2:end,analysisIdx) = dataStruct(i).stimulus_names;
        modelAnswers(2:end,1,analysisIdx) = dataStruct(i).response_names;
        for k = 2:size(modelAnswers,1)
            for l = 2:size(modelAnswers,2)
                modelAnswers{k,l,analysisIdx} ...
                    = absolute_modelanswer(k-1,l-1);
            end
        end

        % Compute maximum parameter model and perform a model test
        fprintf([repelem('-',80) '\n']);

        % The test's df are the difference between the max. param. model's
        % degrees of freedom and the restricted model's degrees of freedom.
        df_model = get_df_model([responseNumber, meanNumber], cur_th.type);
        [LLH_maxparam, ddf, hyp] ...
            = maxparam_lr_test(corrected_data, model_logli, df_model);
        %df = ((num_responses-1)*num_means) - (num_means-1+num_criteria);
        %[max_param_logli,hyp] = maxparamloglike(corrected_data, model_logli, df);

        maxParamLLH(analysisIdx) = LLH_maxparam;
        dfArray(analysisIdx)     = ddf;
        hyp_cell{analysisIdx}    = hyp;

        analysisNames(analysisIdx,1) = {dataStruct(i).name};
        analysisNames(analysisIdx,2) = {cur_th.name};

        if plot_switch
            % Generate a plot of the gaussian distributions and criteria
            fprintf([repelem('-',80) '\n']);
            rel_stims   = sum(corrected_data,1)/sum(sum(corrected_data,1));
            [~,name,~]  = fileparts(filename);
            plotname    = [name '_' analysisNames{analysisIdx,1} '_' ...
                analysisNames{analysisIdx,2}];
            plottitle   = [name ' | ' analysisNames{analysisIdx,1} ...
                ' | ' analysisNames{analysisIdx,2}];

            if ~exist('./plot/', 'dir')
                mkdir('./plot/');
            end

            % Create and write plot.
            plot_model(dataStruct.data, model_means, model_criteria, rel_stims, ...
                dataStruct(i).stimulus_names, ...
                dataStruct(i).response_names, ...
                plottitle, plotname);

            fprintf('Plot written to file:\n\t%s\n', ...
                ['./plot/' plotname '.png']);
        end

        analysisIdx = analysisIdx + 1;
    end % analyse theories

    for m = 1:length(dataStruct(i).tests)
        fprintf([repelem('-',80) '\n']);
        cur_test    = dataStruct(i).tests(m);
        name1       = lower(cur_test.less_df_name);
        name2       = lower(cur_test.more_df_name);
        fprintf('Testing ''? %s ; %s''...\n', name1, name2);

        th_names    = cellfun(@lower,{dataStruct(i).theories.name},'un',0);
        idx1        = strcmp(name1, th_names);
        idx2        = strcmp(name2, th_names);
        type1       = dataStruct(i).theories(idx1).type;
        type2       = dataStruct(i).theories(idx2).type;

        data_size   = size(dataStruct(i).data);
        df1         = get_df_model(data_size, type1);
        df2         = get_df_model(data_size, type2);

        LLH_idx1 = and(strcmp(dataStruct(i).name,analysisNames(:,1)),...
            strcmp(name1, cellfun(@lower, analysisNames(:,2), 'un',0)));
        LLH_idx2 = and(strcmp(dataStruct(i).name,analysisNames(:,1)),...
            strcmp(name2, cellfun(@lower, analysisNames(:,2), 'un',0)));

        result = likelihood_ratio_test(restrModelLLH(LLH_idx1),...
            restrModelLLH(LLH_idx2), df1, df2, name1, name2);

        LRnames{testIdx,1}  = dataStruct(i).name;
        LRnames{testIdx,2}  = name1;
        LRnames{testIdx,3}  = name2;
        LRdfs(testIdx)      = df2 - df1;
        LRllh1(testIdx)     = restrModelLLH(LLH_idx1);
        LTllh2(testIdx)     = restrModelLLH(LLH_idx2);
        LRresults{testIdx}  = result;
        testIdx             = testIdx + 1;

        fprintf('LR Test gave the result to prefer: "%s"\n', result);
    end

    percentage = (100/length(dataStruct) * i);
    waitbar(percentage/100,pleaseHold, ...
        {sprintf('Analysing set %d/%d', ...
        i, length(dataStruct))});
end
fprintf([repelem('=',80) '\n']);
delete(pleaseHold);
if ~tabWrite % no file results wanted
    return
end
fprintf(['Writing results to disk...\nPlease stand by. ' ...
    'Depending on your machine and the number of analyses this can\n' ...
    'take up to several minutes.\n']);

%% Write results to files.
projectDirectory = [base.home filesep 'projects' filesep base.projectName];
[~,fileName,~] = fileparts(base.dataFile);

% Write tabular results
resultsFileTab = [projectDirectory filesep 'results' filesep fileName '_SDT_tab.csv'];
resultArray = assemble_results(analysisNames, restrModelLLH, ...
    maxParamLLH, dfArray, hyp_cell, criteria, means, scalefacs, exps);
writecell(resultArray, resultsFileTab);

% Write model results
resultFolderModels = [projectDirectory filesep 'results' filesep fileName '_SDT_models'];
mkdir(resultFolderModels);
setNames = cell(size(analysisNames,1),1);
modelNames = cell(size(analysisNames,1),1);
for i = 1:size(analysisNames,1)
    nameSplit = split(analysisNames{i,1}, ";");
    nameSplit = split(nameSplit(1), filesep);
    setNames{i} = char(nameSplit(length(nameSplit)));
end
unique_setNames = unique(setNames);
offset = 0;
for i = 1:size(unique_setNames,1)
    resultFileModels = [projectDirectory filesep 'results' filesep fileName '_SDT_models' filesep unique_setNames{i} '.csv'];
    current_names = analysisNames(cellfun(@(c) contains(c,unique_setNames{1}), analysisNames),:);
    current_answers = modelAnswers(:,:, 1+offset:size(current_names,1)+offset);
    current_results = cell(size(modelAnswers,2), size(current_names, 2));
    for j = 1:size(current_names,1)
        modelName = current_names{j,2};
        if j == 1
            headerLine = cell(1, size(modelAnswers,2));
            headerLine{1} = modelName;
        else
            headerLine = cell(2, size(modelAnswers,2));
            headerLine{2,1} = modelName;
        end
        headerAnswers = cat(1, headerLine, current_answers(:,:,j));
        if j == 1
            current_results = headerAnswers;
        else
            current_results = cat(1, current_results, headerAnswers);
        end
    end
    writecell(current_results, resultFileModels);
    modelNames{i} = [analysisNames{i,1} '_' analysisNames{i,2}];
    offset = size(analysisNames,1)/size(unique_setNames,1)*i;
end
resultArchiveModels = [projectDirectory filesep 'results' filesep fileName '_SDT_models.zip'];
zip(resultArchiveModels,resultFolderModels);
rmdir(resultFolderModels, 's');

% Write LR test results
resultFileTest = [projectDirectory filesep 'results' filesep fileName '_SDT_test.csv'];
resultArray = cell(testNumber+1, 7);
resultArray(1,:) = {'Name','Theory 1','Theory 2','Test df',...
    'logli_1','logli_2','Result'};

for i = (1:testNumber)+1
    setNames = split(LRnames{i-1,1}, ";");
    setNames = split(setNames(1), filesep);
    setNames = setNames(length(setNames));
    resultArray{i,1} = string(setNames);
    resultArray{i,2} = LRnames{i-1,2};
    resultArray{i,3} = LRnames{i-1,3};
    resultArray{i,4} = LRdfs(i-1);
    resultArray{i,5} = LRllh1(i-1);
    resultArray{i,6} = LTllh2(i-1);
    resultArray{i,7} = LRresults{i-1};
end
writecell(resultArray, resultFileTest);

fprintf([repelem('=',80) '\n']);
fprintf('All results written to files:\n\t%s\n\t%s\n\t%s\n\n', ...
    resultsFileTab, resultFileTest, resultArchiveModels);
diary OFF
end % fct

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function classification = find_classification(entry)
% Returns a matrix of 1's and 0's matching a single trial's dimensions
% and specifying which stimuli belong to which responses.
rn = entry.response_names;
tr = entry.target_responses;
classification = nan(length(rn),length(tr));
for row = 1:length(rn)
    classification(row,:) = strcmp(tr,rn{row});
end
end

function df_model = get_df_model(data_size, theory_type)

respno = data_size(1);
critno = respno - 1;
stimno = data_size(2);

switch theory_type
     case 'poisson'
        df_model = stimno + critno;
    case 'gauss'
        df_model = stimno - 1 + critno;
    case 'free' % remove before publishing, but keep now for maintenance purposes
        df_model = stimno - 1 + critno;
    case 'linear' % scale factor
        df_model = 1 + critno;
    case 'power' % scale factor + exponent
        df_model = 1 + 1 + critno;
    case 'absolut'
        df_model = critno;
    case 'total'
        df_model = 0;
end
end % fct

function result_cell = assemble_results(analysis_names, restr_loglis,...
    max_param_loglis, df_list, hyp_cell, criteria, means, scalefacs, exps)
% Combines data given in the arguments to a single result table that
% can be stored in a csv file.
if ~all(size(analysis_names,1) == [length(restr_loglis) ...
        length(max_param_loglis) length(df_list) ...
        length(hyp_cell) size(criteria,1) size(means,1)])
    error('Dimension mismatch for the analysis results!')
end

% Find the maximum number of tags given behind the identifier
max_conditions = 0;
for i = 1:size(analysis_names,1)
    tmp = analysis_names{i,1};
    tmp = strsplit(tmp, ';');
    num_conditions = (length(tmp)-1);
    if num_conditions > max_conditions
        max_conditions = num_conditions;
    end
end

% Initialise result cell (lenghts of 1D-fields given for explicity)
result_cell = cell(size(analysis_names,1) + 1, ...
    length(analysis_names{1,1})+ max_conditions ...
    + length(restr_loglis(1)) + length(max_param_loglis(1)) ...
    + length(df_list(1)) + length(hyp_cell(1)) ...
    + length(criteria(1,:)) + length(means(1,:)));

% Write headers
result_cell{1,1} = 'ID';
for j = 1:max_conditions
    result_cell{1,j+1} = ['cond_' int2str(j)];
end
result_cell{1,max_conditions+2} = 'Theory';
result_cell{1,max_conditions+3} = 'Logli SDT';
result_cell{1,max_conditions+4} = 'Logli Maxparam';
result_cell{1,max_conditions+5} = 'df';
result_cell{1,max_conditions+6} = 'Hypothesis';
num_criteria = size(criteria,2);
for j = 1:num_criteria
    result_cell{1,j+max_conditions+6} = ['c^' int2str(j) '_' int2str(j+1)];
end
result_cell{1,max_conditions+7+num_criteria} = 'd'' Scale Factor';
result_cell{1,max_conditions+8+num_criteria} = 'd'' Exponent';
for j = 1:size(means,2)
    result_cell{1,j+max_conditions+8+num_criteria} = [char(181) '_' int2str(j)];
end

% Fill in data
for i = 1:size(analysis_names,1)
    nameSplit = strsplit(analysis_names{i,1},';');
    nameSplit = split(nameSplit(1), filesep);
    nameSplit = nameSplit(length(nameSplit));
    for j = 1:(max_conditions+1)
        if length(nameSplit) >= j
            result_cell{i+1,j} = char(nameSplit(j));
        else
            result_cell{i+1,j} = '-';
        end
    end
    result_cell{i+1,max_conditions+2} = analysis_names{i,2};
    result_cell{i+1,max_conditions+3} = restr_loglis(i);
    result_cell{i+1,max_conditions+4} = max_param_loglis(i);
    result_cell{i+1,max_conditions+5} = df_list(i);
    result_cell{i+1,max_conditions+6} = hyp_cell{i};
    for j = 1:num_criteria
        result_cell{i+1,j+max_conditions+6} = criteria(i,j);
    end
    result_cell{i+1,max_conditions+7+num_criteria} = scalefacs(i);
    result_cell{i+1,max_conditions+8+num_criteria} = exps(i);
    for j = 1:size(means,2)
        result_cell{i+1,j+max_conditions+8+num_criteria} = means(i,j);
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AUX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function corrected_data = add_trial(addTrial, uncorrected_data)
% correct the data matrix by adding a "random" trial to each column
% containing a zero value
% no no no... add trial anyway!
corrected_data = uncorrected_data;
num_responses = size(uncorrected_data,1);
% [~, zero_cols] = find(uncorrected_data == 0);
% corrected_data(:,zero_cols) = corrected_data(:,zero_cols) ...
%     + addTrial/num_responses;
corrected_data(:,:) = corrected_data(:,:) ...
                              + addTrial/num_responses;
end % fct