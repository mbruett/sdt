% Demonstration für die SDT-Analyse.
%
% Das vorliegende Programm wertet Excel-Dateien getrennt voneinander aus
% und speichert die Ergebnisse sowohl für die Gesamtdaten als auch für jede
% einzelne Versuchsperson in eine weitere Excel-Datei, die unter dem Namen
% "<alter Dateiname>_SDT.xls" in das Quellverzeichnis gespeichert wird.

clear
% Generate a 'home' directory to start at and come back to after
% finishing a project, run through, or error.
base.home = pwd;
% time of program start
base.startTime = string(datetime("now",'Format','yyyy-MM-dd_HH-mm-ss'));

try
    %% Create necessary folder structure if not already in place.
    clear exist;    %failsave for possible workspace issues
    % Check for/Create folder projects are saved to.
    if exist('projects', 'dir') == 0
        mkdir projects;
    end
    % Set directory of current project(bc no project has been started yet,
    % it is set to the base projects folder.
    projectDirectory = fullfile(base.home, 'projects');
    cd (projectDirectory);

    projectMenu = questdlg(['Do you want to create a new project' ...
        ' or continue working on an existing one?'], ...
        'SENS', 'Create', ...
        'Continue', 'Create');


    nameMenu = inputdlg('Project name:','Name',[1 70]);
    % Check if name already exists (see nameFolder.m).
    % Save nameFolder output as new project name.
    if projectMenu == "Create"
        base.projectName = nameFolder(nameMenu{1});
    else
        base.projectName = nameMenu{1};
    end
    % Save project directory.
    projectDirectory = fullfile(base.home, 'projects', base.projectName);
    cd(projectDirectory);

    switch projectMenu
        case 'Create'
            % Create subfolders.
            mkdir 'data';
            mkdir 'results';
            mkdir 'log';
            mkdir 'plot';
            % Go back to project directory.
            cd(projectDirectory);
            % Get data from archive.
            cd(base.home);
            [base.dataFile,dataFilePath] = ...
                uigetfile({'*.zip'; '*.csv'},'Select the archive containing your data.',...
                'data');
            cd(fullfile(projectDirectory, 'data'));
            % Copy selected file into the new project infrastructure.
            copyfile(fullfile(dataFilePath, base.dataFile));

            % Handle which output is created.
            tabWrite = 0; tabString = "off";
            plotSwitch = 0; plotString = "off";
            output = questdlg(['Do you want to create Output aside from the log/command window?'],'Output Options', 'Yes', 'No', 'No');
            % Ask which output should be created.
           switch output
               case 'Yes'
                   options = questdlg(['Your results can be saved as a table, plotted or both. ' ...
                       'Please select the output you would like to create for this data set.'], ...
                       'Output Options', 'Table', 'Plot', 'Both', 'Table');
                   switch options
                       case 'Plot'
                           plotSwitch = 1;
                           plotString = "on";
                       case 'Table'
                           tabWrite = 1;
                           tabString = "on";
                       case 'Both'
                           plotSwitch = 1;
                           plotString = "on";
                           tabWrite = 1;
                           tabString = "on";
                   end
           end
        case 'Continue'
            % Check if the data archive exists.
            cd([projectDirectory filesep 'data'])
            if isempty(dir('*.zip')) % no .zip file found
                error(['This project does not have a fitting file to get data from.'...
                    'Please create a new project and select a .zip file to analyse.'])
            else
                % Get data on zip files.
                folderContent = dir('*.zip');
                if size(folderContent,1) == 1
                    % Set file name.
                    base.dataFile = folderContent.name;
                else % more than one zip archive in folder
                    selectionTF = 0;
                    while selectionTF == 0
                        % Let user decide which file should be used.
                        [fileIdx, selectionTF] = listdlg('PromptString', {'There is more than one possible file in the folder.', 'Please select the one you would like to analyse.'}, ...
                            'SelectionMode','single', ...
                            'ListString', {folderContent.name});
                    end
                    % Set file name.
                    base.dataFile = folderContent(fileIdx).name;
                end
                cd(projectDirectory);
                % Check if the parameter file exists.
                infoFile = sprintf('%s_parameters.txt', base.projectName);
                if isfile(infoFile)
                    parameterWrite = fopen(infoFile);
                    % Scan the text file for the parameters.
                    lines = textscan(parameterWrite, '%s', ...
                        'Delimiter', '\n'); % line breaks
                    lines = lines{1};
                    % Delete empty cells.
                    lines = lines(cellfun(@(c) ~isempty(c), lines));
                    % Isolate value.
                    lines = split(lines(6:end), ':');
                    lines = split(lines(:,2), ' ');
                    lines = lines(:,2);

                    tabString       = lines{1}; % tag for tab writing
                    plotString      = lines{2}; % tag for plot writing
                    addTrial        = str2num(lines{3}); % number of trials to be added
                    if length(lines) > 3
                        poissMu = lines{4};
                    end

                    % Convert tags to numeric/logical values.
                    for i = 1:length(lines)
                        if lines{i} == "off"
                            lines{i} = 0;
                        elseif lines{i} == "on"
                            lines{i} = 1;
                        end
                    end

                    tabWrite    = lines{1}; % logical value for table writing
                    plotSwitch  = lines{2}; % logical value for plot writing

                    % Close the file.
                    fclose(parameterWrite);
                else
                    error(['This project does not have a fitting file to get the required parameters from. '...
                        'Please create a new project to decide how you would like your data to be processed.'])
                end
            end
    end

    % Get name of archive without extension.
    [~,fileName,~] = fileparts(base.dataFile);

    % Set name of log file.
    logName = sprintf('%s_%s.log', base.projectName, base.startTime);
    % Set path to log file.
    logPath = fullfile(projectDirectory, 'log', logName);
    % Check if there is already a file with that name.
    if exist(logPath, 'file') == 2
        delete(logPath);
    end
    % Set log to follow all subsequent cw output.
    logID = fopen(logPath,'wt');
    fclose(logID);
    diary(logPath);

    cd(projectDirectory);

    %% Get data from file archive.
    dataStruct = get_data_csv(base,'');

    %% Write additional parameters to file, if the project is new.
    if projectMenu == "Create"
        cd(projectDirectory);
        % Open file to save base info to.
        parameterWrite = fopen(sprintf('%s_parameters.txt', base.projectName), 'w');
        % Set the text of the file
        formatSpec = ['IMPORTANT PARAMETERS:\n\n' ...
            'Number of Sets:\t%d' ...
            '\n\nStimulus names:\t[%s]' ...
            '\nResponse names:\t[%s]' ...
            '\nTheories:\t[%s]' ...
            '\n\nWrite to file: %s' ...
            '\nWrite to plot: %s'];
        stimString = sprintf(repmat('%s / ', 1, numel(dataStruct(1).stimulus_names)),string(dataStruct(1).stimulus_names));
        respString = sprintf(repmat('%s / ', 1, numel(dataStruct(1).target_responses)),string(dataStruct(1).target_responses));
        theoryString = sprintf(repmat('%s / ', 1, numel({dataStruct(1).theories.name})),string({dataStruct(1).theories.name}));
        % Add the values to their place.
        fprintf(parameterWrite, formatSpec, length(dataStruct), ...
            stimString(1:length(stimString)-3), respString(1:length(respString)-3), ...
            theoryString(1:length(theoryString)-3), tabString, plotString);
        fclose(parameterWrite);

        parameterAppend = fopen(sprintf('%s_parameters.txt', base.projectName), 'a');
        if ismember('poisson',{dataStruct(1).theories.type}) % poisson is theory type
            additions = inputdlg({'Mean for the noise distribution of the Poisson model:', ...
                'Number of trials to add to the data:'}, 'Poisson', [1 70], {'0.5', '1'});
            poissMu = str2num(additions{1});
            addTrial = str2num(additions{2});
            formatSpec = ['\n\nAdditional Trials: %d' ...
                '\n\nMean for Poisson model: %g'];
            % Add the values to their place.
            fprintf(parameterAppend, formatSpec, addTrial, poissMu);
        else % no poisson
            addTrial = str2num(string(inputdlg('Number of trials to add to the data:', 'Trials', [1 70], {'1'})));
            formatSpec = ['\n\nAdditional Trials: %d'];
            % Add the values to their place.
            fprintf(parameterAppend, formatSpec, addTrial);
        end
        fclose(parameterAppend);
    end

    %% Start analysis.
    if ismember('poisson',{dataStruct(1).theories.type})
        sdtbatch(dataStruct, base, addTrial, plotSwitch, tabWrite, poissMu)
    else
        sdtbatch(dataStruct, base, addTrial, plotSwitch, tabWrite, [])
    end
    % Go to 'home' directory
    cd(base.home);
catch ME
    % Go to 'home' directory
    cd(base.home);
    % Rethrow the error message Matlab would've given out.
    causeException = MException('MATLAB:myCode:dimensions', ...
        ['If this error persists please contact the support ' ...
        '(schellnock@psychologie.uni-kiel.de).']);
    ME = addCause(ME,causeException);
    rethrow(ME);
end
