function poolmodel
% simulate some d' model output with variations
% and how one could pool this model output

N = 50;
true = 0:.2:1;  % true d'L/d'T values

mintarget = 2;
maxtarget = 3;  % limits for (uniform) distribution of d'T
noise = .5;      % some noise to be added to individual d'

deprivec = nan(N,length(true));
normdeprivec = deprivec;

for n=1:N
    dprime = mintarget + rand * (maxtarget-mintarget);  % jeder Jeck ist anders
    truedeprivec = true * dprime;
    deprivec(n,:) = truedeprivec + (2*rand(size(true))-1) * noise;
    deprivec(n,:) = deprivec(n,:) - deprivec(n,1);
    normdeprivec(n,:) = deprivec(n,:)/deprivec(n,end);
end

meannormdeprivec = mean(normdeprivec);
stdnormdeprivec  = std (normdeprivec) / sqrt(N);

meandeprivec = mean(deprivec);
stddeprivec  = std (deprivec) / sqrt(N);
meandeprivec = meandeprivec/meandeprivec(end);
stddeprivec  =  stddeprivec/meandeprivec(end);

figure;
hold on;
errorbar(true-0.01,meannormdeprivec,stdnormdeprivec,'color','k','marker','o','linestyle','none');
errorbar(true+0.01,meandeprivec,stddeprivec,'color','r','marker','o','linestyle','none');
plot([0 1],[0 1],'-k')
xlim([0 1])
ylim([0 1])
set(gca,'dataaspectratio',[1 1 1],'clipping','off','xtick',true,'ytick',true);

