function answers = randomdata(seed,stimno,trialno,respno)

% randomdata(stimno,respno,xlsfile)
%
% seed      Seed for random number generator
% stimno    Number of stimuli
% trialno   Number of trials per stimulus
% respno    Number of responses
% xlsfile   where the random data should go
%           They go there into four sheets:
%           first means of simulated means and criteria
%           then ordered simulated data
%           then several sheets with randomized stimuli
%           then several sheets with randomized stimuli and answers

path = 'xlslog/randomdata';
file = sprintf('%s/RD %d stim (%d trials), %d resp, seed=%3.3d',...
    path,stimno,trialno,respno,seed);
xlsfile = [file '.xlsx'];
tifffile = [file '.tiff'];

rng(seed);

dprimemax = 4;      % Maximum dprime (theoretically)
mu = dprimemax * rand(1,stimno);
mu = mu - min(mu);  % the smallest of the mu should be equal to zero
mu = sort(mu);

critminmax = 2;     % range of criteria relative to min(mu) and max(mu)
mincrit = min(mu)-critminmax;
maxcrit = max(mu)+critminmax;
crit = mincrit + rand(1,respno-1)*(maxcrit-mincrit);
crit = sort(crit);
critno = length(crit);

answers = zeros(respno,stimno);
for m = 1:stimno
    event = mu(m) + randn(1,trialno);
    event = sort(event);
    answers(1,m) = length(find(event<crit(1)));
    for c=2:critno
        answers(c,m) = length(find(event>=crit(c-1)&event<crit(c)));
    end
    answers(end,m) = length(find(event>=crit(end)));
end

disp(answers)

x0 = mincrit - critminmax;
x1 = maxcrit + critminmax;
xx = x0:.01:x1;
hold on
for s=1:stimno
    yy = normpdf(xx,mu(s),1);
    plot(xx,yy,'-k');
    yy = 0.45 - mod(s,2)*0.023;
    text(mu(s),yy,sprintf('%.2f',mu(s)),'hor','center');
end
yyy = 0.48;
acol = 'r';
text(crit(1)-1,yyy,sprintf('A%d',1),'hor','center','color',acol);
for c=1:critno
    plot(crit(c)*[1 1],[0 0.5],'-r');
    if c>1
        xxx = (crit(c) + crit(c-1)) / 2;
        text(xxx,yyy,sprintf('A%d',c),'hor','center','color',acol);
    end
end
text(crit(end)+1,yyy,sprintf('A%d',respno),'hor','center','color',acol);
saveas(gcf,tifffile);

warning('OFF', 'MATLAB:xlswrite:AddSheet')
%%% Sheet 1
xlswrite(xlsfile,{'Mittelwerte'},1,'A1'); xlswrite(xlsfile,mu,1,'B1')
xlswrite(xlsfile,{'Kriterien'},1,'A2');   xlswrite(xlsfile,crit,1,'B2')

%%% Stimulus names
stimname = cell(1,stimno);
for s=1:stimno
    stimname{s} = sprintf('Stim %.2f',mu(s));
end

%%% Response names
respname = cell(respno,1);
for r=1:respno
    respname{r,1} = sprintf('Ans %d',r);
end

sheet = 'ordered';
xlswrite(xlsfile,stimname,sheet,'B1')
xlswrite(xlsfile,respname,sheet,'A2')
xlswrite(xlsfile,answers,sheet,'B2')

for r=1:6
    sheet = sprintf('rand stim %d',r);
    muorder = randperm(stimno);
    ranswers = answers(:,muorder);
    xlswrite(xlsfile,stimname(muorder),sheet,'B1')
    xlswrite(xlsfile,respname,sheet,'A2')
    xlswrite(xlsfile,ranswers,sheet,'B2')
end

for r=1:6
    sheet = sprintf('rand stim&ans %d',r);
    muorder = randperm(stimno);
    ansorder = randperm(respno);
    ranswers = answers(ansorder,muorder);
    xlswrite(xlsfile,stimname(muorder),sheet,'B1')
    xlswrite(xlsfile,respname(ansorder),sheet,'A2')
    xlswrite(xlsfile,ranswers,sheet,'B2')
end
