% get some answer matrix
f = figure;
tic
a = randomdata(9,5,1000,3);
toc
delete(f);
a %#ok<*NOPTS>

% get permuted answer matrix
permute_mu = randperm(5);
perm_a = a(:,permute_mu);
perm_a

startparam = getstart(a);
startparam

mu = [0 startparam(3:end)];
mu

perm_startparam = getstart(perm_a);
perm_startparam

perm_mu = [0 perm_startparam(3:end)];
perm_mu

permuted_mu = mu(permute_mu);
permuted_mu

diff = permuted_mu - perm_mu;
diff

